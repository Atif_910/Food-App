import Footer from "../Layout/Footer/Footer";
import Header from "../Layout/Header/Header";

import classes from './Home.module.css';
// importing Images
import bannerImg from '../../Assests/Images/meals.jpg';

const Home = () => {


    return (
        <div className={`${classes.homeDiv}`}>
            <Header />
            <div className={`${classes.bannerDiv}`}>
                <img src={bannerImg} alt={bannerImg + " Image"} />
            </div>
            <Footer />
        </div>
    )
};

export default Home;