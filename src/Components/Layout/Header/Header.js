import classes from './Header.module.css';

const Header = () => {


    return (
        <div className={`${classes.header}`}>
            <nav>
                <li className={`${classes.top_heading}`}>ReactMeals</li>
            </nav>
            <nav>
                <button>Your Cart <span className={`${classes.btn_span}`}>0</span></button>
            </nav>
        </div>
    )
}

export default Header;