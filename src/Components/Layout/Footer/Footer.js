import classes from './Footer.module.css';


const Footer = () => {
    return (
        <div className={`${classes.footer}`}>
            <h>Footer Works</h>
        </div>
    )
}

export default Footer;